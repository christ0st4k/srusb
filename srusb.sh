#!/bin/bash

info_devs=$(lsblk -o PATH,MODEL,MOUNTPOINT | grep "/media")

if [ -z "$info_devs" ]; then
  echo "No USB devices connected to your machine"
else
  echo -e "Connected USB Devices:"
  lsblk -o NAME,PATH,SIZE,MODEL,MOUNTPOINT,FSTYPE | grep "/media"
  mounted_devs=($(lsblk -o PATH,MOUNTPOINT | grep "/media" | awk '{print $1}')) # Added parenthesis to solve array indexing
  # mounted_paths=($(lsblk -o PATH,MOUNTPOINT | grep "/media" | awk '{print $2}'))  PROBABLY UNNECESSARY
  echo -e "\nSelect the number on the left of the device to safely remove it:\n"
  i=$(echo "${#mounted_devs[@]}")
  for ((dev=0; dev <$i ; dev++)); do
    echo "[$((dev+1))]" "${mounted_devs[dev]}"
  done
  len=$(expr length  "${mounted_devs[0]}")
  
  while true; do
    read -rp "[Q/q for exit]:" opt
    if [ "$opt" == "Q" ] || [ "$opt" == "q" ]; then
      exit 0
    elif [ "$opt" -le "$i" ]; then
      pids=($(lsof "${mounted_devs[$opt-1]}" | awk '{print $2}'))
      echo "$(lsof "${mounted_devs[$opt-1]}" | awk '{print $2}')" > /tmp/tmplist.txt
      if [ -z "$pids" ]; then
        udisksctl unmount -b "${mounted_devs[$opt-1]}"
        if [ "$len" -eq 9 ]; then
          udisksctl power-off -b "${mounted_devs[$opt-1]%?}" && echo "And powered it off"
          exit 0
        else
          udisksctl power-off -b "${mounted_devs[$opt-1]}" && echo "And powered it off"
          exit 0
        fi
      else
	echo -e "The device you selected is being used by those pid's:\n$(cat /tmp/tmplist.txt)" #"${pids[*]}"
	echo -e "\nProcess info:"
	ps -aux | grep $(echo "$(sed 's/PID//' /tmp/tmplist.txt)") | grep -v grep
	echo -e "\nTerminate that process and run again the command\nYou can try 'kill -9 $(sed 's/PID//' /tmp/tmplist.txt)'" 
	break
      fi
    else
      echo "Bad option"
    fi
  done
fi
